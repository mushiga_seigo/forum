package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView searchReport(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		// 絞り込み期間を取得
				String startDate = request.getParameter("startDate");
		        String endDate = request.getParameter("endDate");
				// 投稿と返信を取得
		        List<Report> contentData = reportService.searchReport(startDate, endDate);
		        List<Comment> commentData = commentService.findAllComment();
		        // 画面遷移先を指定
		     	mav.setViewName("/top");
		     	// 取得データをセット
		     	mav.addObject("startDate", startDate);
		        mav.addObject("endDate", endDate);
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		Date dateObj = new Date();
		report.setCreated_date(dateObj);
		report.setUpdated_date(dateObj);
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿削除
	@GetMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 投稿を削除
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面表示
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 投稿を取得
		Report report = reportService.editReport(id);
		// 投稿データオブジェクトを保管
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PostMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		Report reportValue = reportService.editReport(id);
		report.setCreated_date(reportValue.getCreated_date());
		report.setUpdated_date(reportValue.getUpdated_date());
		// 投稿を更新
		reportService.updateReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 返信処理
	@PostMapping("/addComment/{report_id}")
	public ModelAndView addComment(@PathVariable Integer report_id, @ModelAttribute("formModel") Comment comment) {
		// 返信をテーブルに格納
		Date dateObj = new Date();
		comment.setCreated_date(dateObj);
		comment.setUpdated_date(dateObj);
		commentService.saveComment(comment);
		// 投稿の日時を更新
		Report report = reportService.editReport(report_id);
		report.setUpdated_date(dateObj);
		reportService.updateReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 返信編集画面表示
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 投稿を取得
		Comment comment = commentService.editComment(id);
		// 投稿データオブジェクトを保管
		mav.addObject("comment", comment);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		return mav;
	}

	// 返信編集処理
	@PostMapping("/updateComment/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		// 返信を取得
		Comment getComment = commentService.editComment(id);
		// report_idを取得してセット
		comment.setReport_id(getComment.getReport_id());
		comment.setCreated_date(getComment.getCreated_date());
		comment.setUpdated_date(getComment.getUpdated_date());
		// 投稿を更新
		commentService.updateComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿削除
	@GetMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// 投稿を削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}