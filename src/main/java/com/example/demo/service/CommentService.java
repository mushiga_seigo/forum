package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// 返信全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllComment();
	}

	// 返信追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 返信1件取得
	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	// 返信更新
	public void updateComment(Comment comment) {
		commentRepository.save(comment);
	}

	// レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
