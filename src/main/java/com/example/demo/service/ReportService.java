package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAllReport();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = reportRepository.findById(id).orElse(null);
		return report;
	}

	// レコード更新
	public void updateReport(Report report) {
		reportRepository.save(report);
	}

	// レコード絞り込み
	public List<Report> searchReport(String startDate, String endDate) {
		List<Report> report = null;
		if ((StringUtils.hasLength(startDate)) & (StringUtils.hasText(startDate))) {
        	startDate += " 00:00:00";
        } else {
        	String defaultDate = "2020-01-01 00:00:00";
        	startDate = defaultDate;
        }

        if ((StringUtils.hasLength(endDate)) & (StringUtils.hasText(endDate))) {
        	endDate += " 23:59:59";
        } else {
        	//現在日時を取得
            Date dateObj = new Date();
    		SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    		// 日時情報を指定フォーマットの文字列で取得
    		String nowDate = format.format( dateObj );
        	endDate = nowDate;
        }
        Timestamp stDate = Timestamp.valueOf(startDate);
    	Timestamp enDate = Timestamp.valueOf(endDate);
        report = reportRepository.findByReport(stDate, enDate);
		return report;
	}
}
